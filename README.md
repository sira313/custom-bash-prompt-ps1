# Custom Bash Prompt PS1

![hm-custom-PS1](https://1.bp.blogspot.com/-rAftF9zplvQ/W-rbI6_oKLI/AAAAAAAAAAM/2LMRfev9H-Ua-VCeAmSyR0dewhxetJ2iQCLcBGAs/s1600/Screenshot_20181113_210837.png)

Just copy [this](https://gitlab.com/habibmustofa/custom-bash-prompt-ps1/blob/master/hm-bash-prompt.sh) script to your **.bashrc** and reload your terminal.

You can change colors and unique character whatever you want! 🤪
